#!/usr/bin/python

import time, os, sys

filename = "VID_" + time.strftime('%Y%m%d_%H%M%S')

os.system("sudo raspivid -fps 25 -w 1280 -h 720 -rot 180 -o media/" + filename + ".h264 -t " + sys.argv[1] + "000")
os.system("MP4Box -fps 25 -add media/" + filename + ".h264 media/" + filename + ".mp4")
os.system("Dropbox-Uploader/dropbox_uploader.sh upload media/" + filename  + ".mp4 " + filename + ".mp4")
os.remove("media/" + filename + ".h264")
os.remove("media/" + filename + ".mp4")
os.system("curl -X POST https://maker.ifttt.com/trigger/video/with/key/o7hEJxAYX9g1PfE5w6ysX")
