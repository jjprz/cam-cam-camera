#!/usr/bin/python

import os, sys, time, shutil

filename = time.strftime('%Y%m%d_%H%M%S')
serial = 0

os.mkdir("media/" + filename)

for i in range(int(sys.argv[1])):
    serialnumber = "%05d" % (serial)
    os.system("sudo raspistill -rot 180 -o media/" + filename + "/" + serialnumber + ".jpg -sh 50 -mm average -ex night")
    serial += 1
    time.sleep(60)

os.chdir("media/" + filename)
os.system("ls > listnames.txt")
os.system("mencoder -o ../TIMELAPSE_" + filename + ".avi -mf type=jpeg:fps=25 mf://@listnames.txt -vf scale=1920:1080 -nosound -lavcopts vcodec=mpeg4:aspect=16/9:vbitrate=8000000 -ovc lavc")
shutil.rmtree("../" + filename)
#os.remove("TIMELAPSE_" + filename + ".avi")
os.system("curl -X POST https://maker.ifttt.com/trigger/time_lapse/with/key/o7hEJxAYX9g1PfE5w6ysX")
