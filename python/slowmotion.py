#!/usr/bin/python

import time, os, sys

filename = "SLOWMOTION_" + time.strftime('%Y%m%d_%H%M%S')

os.system("sudo raspivid -rot 180 -w 640 -h 480 -fps 90 -o media/" + filename + ".h264 -t " + sys.argv[1] + "000")
os.system("MP4Box -fps 25 -add media/" + filename + ".h264 media/" + filename + ".mp4")
os.system("Dropbox-Uploader/dropbox_uploader.sh upload media/" + filename + ".mp4 " + filename + ".mp4")
os.remove("media/" + filename + ".h264")
os.remove("media/" + filename + ".mp4")
os.system("curl -X POST https://maker.ifttt.com/trigger/slow_motion/with/key/o7hEJxAYX9g1PfE5w6ysX")
