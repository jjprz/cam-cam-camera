#!/usr/bin/python

import time, os

filename = "IMG_" + time.strftime('%Y%m%d_%H%M%S') + ".jpg"

os.system("sudo raspistill -rot 180 -o media/" + filename)
os.system("Dropbox-Uploader/dropbox_uploader.sh upload media/" + filename + " " + filename)
os.remove("media/" + filename)
os.system("curl -X POST https://maker.ifttt.com/trigger/photo/with/key/o7hEJxAYX9g1PfE5w6ysX")
