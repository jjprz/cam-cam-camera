<html>
<head>
	<meta charset="UTF-8">
	<title>Cam Cam Camera</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="img/favicon.png">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body>
	<form action="" method="post">
		<button type="submit" name="photo" id="photo">
	  		<i class="material-icons">photo_camera</i>
		</button>
		<button type="submit" name="video" id="video">
	  		<i class="material-icons">videocam</i>
		</button>
		<button type="submit" name="slowmotion" id="slowmotion">
	  		<i class="material-icons">slow_motion_video</i>
		</button>
		<button type="submit" name="timelapse" id="timelapse">
	  		<i class="material-icons">timelapse</i>
		</button>
	</form>
	<img src="img/logo.png" id="logo">
	<style type="text/css">
		body, html {
			background-color: #c7053d;
		}
		button, button:active {
			width: 25%;
			height: 150px;
			background-color: #8cc04b;
			box-shadow: 1px 0px #507128,   0px 1px #507128,   2px 1px #507128,   1px 2px #507128,
						3px 2px #507128,   2px 3px #507128,   4px 3px #507128,   3px 4px #507128,
						5px 4px #507128,   4px 5px #507128,   6px 5px #507128,   5px 6px #507128,
						7px 6px #507128,   6px 7px #507128,   8px 7px #507128,   7px 8px #507128,
						9px 8px #507128,   8px 9px #507128,   10px 9px #507128,  9px 10px #507128,
						11px 10px #507128, 10px 11px #507128, 12px 11px #507128, 11px 12px #507128,
						13px 12px #507128, 12px 13px #507128, 14px 13px #507128, 13px 14px #507128,
						15px 14px #507128, 14px 15px #507128, 16px 15px #507128, 15px 16px #507128,
						17px 16px #507128, 16px 17px #507128, 18px 17px #507128, 17px 18px #507128,
						19px 18px #507128, 18px 19px #507128, 20px 19px #507128, 19px 20px #507128,
						21px 20px #507128, 20px 21px #507128, 22px 21px #507128, 21px 22px #507128,
						23px 22px #507128, 22px 23px #507128, 24px 23px #507128, 23px 24px #507128,
						25px 24px #507128, 24px 25px #507128, 26px 25px #507128, 25px 26px #507128,
						27px 26px #507128, 26px 27px #507128, 28px 27px #507128, 27px 28px #507128,
						29px 28px #507128, 28px 29px #507128, 30px 29px #507128, 29px 30px #507128;
			margin-top: 6%;
			margin-left: 16%;
			border: none;
			border-radius: 5px;
		}
		button:hover {
			cursor: hand;
			position: relative;
			top: 5px;
			left: 5px;
			box-shadow: 1px 0px #507128,   0px 1px #507128,   2px 1px #507128,   1px 2px #507128,
						3px 2px #507128,   2px 3px #507128,   4px 3px #507128,   3px 4px #507128,
						5px 4px #507128,   4px 5px #507128,   6px 5px #507128,   5px 6px #507128,
						7px 6px #507128,   6px 7px #507128,   8px 7px #507128,   7px 8px #507128,
						9px 8px #507128,   8px 9px #507128,   10px 9px #507128,  9px 10px #507128,
						11px 10px #507128, 10px 11px #507128, 12px 11px #507128, 11px 12px #507128,
						13px 12px #507128, 12px 13px #507128, 14px 13px #507128, 13px 14px #507128,
						15px 14px #507128, 14px 15px #507128, 16px 15px #507128, 15px 16px #507128,
						17px 16px #507128, 16px 17px #507128, 18px 17px #507128, 17px 18px #507128,
						19px 18px #507128, 18px 19px #507128, 20px 19px #507128, 19px 20px #507128,
						21px 20px #507128, 20px 21px #507128, 22px 21px #507128, 21px 22px #507128,
						23px 22px #507128, 22px 23px #507128, 24px 23px #507128, 23px 24px #507128,
						25px 24px #507128, 24px 25px #507128;
		}
		button:focus {
			top: 25px;
			left: 25px;
			outline: 0;
			box-shadow: 1px 0px #507128,   0px 1px #507128,   2px 1px #507128,   1px 2px #507128,
						3px 2px #507128,   2px 3px #507128,   4px 3px #507128,   3px 4px #507128,
						5px 4px #507128,   4px 5px #507128,   6px 5px #c7053d,   5px 6px #c7053d,
						7px 6px #c7053d,   6px 7px #c7053d,   8px 7px #c7053d,   7px 8px #c7053d,
						9px 8px #c7053d,   8px 9px #c7053d,   10px 9px #c7053d,  9px 10px #c7053d,
						11px 10px #c7053d, 10px 11px #c7053d, 12px 11px #c7053d, 11px 12px #c7053d,
						13px 12px #c7053d, 12px 13px #c7053d, 14px 13px #c7053d, 13px 14px #c7053d,
						15px 14px #c7053d, 14px 15px #c7053d, 16px 15px #c7053d, 15px 16px #c7053d,
						17px 16px #c7053d, 16px 17px #c7053d, 18px 17px #c7053d, 17px 18px #c7053d,
						19px 18px #c7053d, 18px 19px #c7053d, 20px 19px #c7053d, 19px 20px #c7053d,
						21px 20px #c7053d, 20px 21px #c7053d, 22px 21px #c7053d, 21px 22px #c7053d,
						23px 22px #c7053d, 22px 23px #c7053d, 24px 23px #c7053d, 23px 24px #c7053d,
						25px 24px #c7053d, 24px 25px #c7053d, 26px 25px #c7053d, 25px 26px #c7053d,
						27px 26px #c7053d, 26px 27px #c7053d, 28px 27px #c7053d, 27px 28px #c7053d,
						29px 28px #c7053d, 28px 29px #c7053d, 30px 29px #c7053d, 29px 30px #c7053d;
		}
		.material-icons {
			font-size: 500%;
			color: white;
		}
		form {
			display: inline;
		}
		#logo {
			position: absolute;
			right: 20px;
			bottom: 20px;
		}
		@media (max-width: 600px) {
			button, button:active {
				width: 30%;
				height: 100px;
				margin: 9%;
				margin-bottom: 12%;
			}
			#logo {
				right: 10px;
				bottom: 10px;
				width: 7%;
			}
		}
	</style>
</body>
</html>
<?php
if (!isset ($_POST['t'])) {
	$t = 10;
} else {
	$t = $_POST['t'];
}
if (isset($_POST['photo'])) {
	shell_exec("python python/photo.py");
} else if (isset($_POST['video'])) {
	shell_exec("python python/video.py " . $t);
} else if (isset($_POST['slowmotion'])) {
	shell_exec("python python/slowmotion.py " . $t);
} else if (isset($_POST['timelapse'])) {
	shell_exec("python python/timelapse.py ". $t);
}
?>